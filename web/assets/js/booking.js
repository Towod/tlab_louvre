
$(document).ready(function(){

    // On récupère le conteneur des formulaires
    var $container = $('#booking_peoples');

    // On définit un compteur pour nos formulaires
    var index = $container.find(':input').length;

    // On ajoute le bouton .add_people
    add_create_btn($container);

    // Si l'index est à 0 alors on ajoute un formulaire
    if( index === 0 ) {
        add_people_form($container);
    } else {
    // Sinon on ajoute un bouton de suppression pour tous les formulaires déjà présents
        $container.children('div').each(function () {
           add_delete_btn($(this));
        });
    }

    function add_people_form($container)
    {
        var template = $container.attr('data-prototype')
            // .replace(/__name__label__/g, 'Personne n°' + (index+1))
            .replace(/__name__label__/g, '')
            .replace(/__name__/g,        index)
        ;

        var $prototype = $(template);

        add_delete_btn($prototype);

        $container.append($prototype);

        index++;
    }

    function add_delete_btn($prototype)
    {
        var $delete_btn = $('<div class="col-3 col-lg-1"><a href="#" class="delete_people btn btn-danger"><i class="fa fa-remove"></i></a></div>');

        $prototype.find('.fields').append($delete_btn);

        $delete_btn.click(function (e) {

            $prototype.remove();

            e.preventDefault();
            e.stopPropagation();

            return false;
        });
    }

    function add_create_btn($container)
    {
        var $create_btn = $('<span class="input-group-addon"><a href="#" class="add_people btn btn-success"><i class="fa fa-plus"></i></a></span> ');

        $('.fa-users').closest('.input-group').append($create_btn);

        $create_btn.click(function (e) {
            add_people_form($container);

            e.preventDefault();
            e.stopPropagation();

            return false;
        });
    }

});