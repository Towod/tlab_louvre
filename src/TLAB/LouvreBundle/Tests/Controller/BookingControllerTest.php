<?php

namespace Tests\LouvreBundle\Controller;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BookingControllerTest extends WebTestCase
{
    public function testBooking()
    {
        // Test de la page d'accueil qui se charge
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Billeterie en ligne', $crawler->filter('h1')->text());

    }

    public function testBookingDate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $form = $crawler->selectButton('Submit')->form();

        $values = $form->getPhpValues();


        $values['booking']['peoples'][0]['firstname'] = 'Yonel';
        $values['booking']['peoples'][0]['lastname'] = 'Becerra';
        $values['booking']['peoples'][0]['country'] = 'FR';
        $values['booking']['peoples'][0]['reducedPrice'] = true;
        $values['booking']['peoples'][0]['birthday'] = '1992-04-22';

        $values['booking']['email'] = 'becerra.mail@gmail.com';
        $values['booking']['date'] = '2017-10-13';
        $values['booking']['type'] = '1';

        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('La date ne peut être antérieure à celle d\'aujourd\'hui', $crawler->filter('li')->text());
    }

}
