<?php

namespace TLAB\LouvreBundle\Controller;

use Stripe\Charge;
use Stripe\Error\Card;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TLAB\LouvreBundle\Entity\Booking;
use TLAB\LouvreBundle\Entity\Paiement;
use TLAB\LouvreBundle\Entity\People;
use TLAB\LouvreBundle\Form\BookingType;
use TLAB\LouvreBundle\Form\PaiementType;
use TLAB\LouvreBundle\Form\PeopleType;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Swift_Message;

use TLAB\LouvreBundle\Validator\hasLowerThanThousandPeoples;


class BookingController extends Controller
{
    /**
     *
     * Affiche le formulaire de réservation et gère quand il est envoyé
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function bookingAction(Request $request)
    {
        $validator = $this->get('validator');


        // On créé une entité Booking
        $booking = new Booking();
        // On crée le formulaire associé à Booking
        $form = $this->get('form.factory')->create(BookingType::class, $booking);

        $formError = '';

        // On récupère la requête lorsque le formulaire est posté
        $form->handleRequest($request);
        // Si les champs du formulaire sont valides
        if($form->isSubmitted() && $form->isValid()) {

            $nbrPeoplesForThisDate = 0;

            // On récupère toutes les commandes pour la date sélectionnée
            $repo = $this->getDoctrine()->getRepository('TLABLouvreBundle:Booking');
            $bookingsForThisDate = $repo->findBy([ 'date'=>$booking->getDate() ]);

            // On additionne toutes les personnes de chaque commande pour cette journée
            foreach ($bookingsForThisDate as $curr_booking) {
                if(!empty($curr_booking->getToken())) {
                    $nbrPeoplesForThisDate += $curr_booking->getNumberOfPeoples();
                }

            }

            // Si le nombre de personnes pour cette journée + le nombre de personnes de la commande dépasse la limite on déclenche une erreur
            $contrainst = new hasLowerThanThousandPeoples($nbrPeoplesForThisDate);
            $formError = $validator->validate($booking->getNumberOfPeoples(), $contrainst);

            // Si il n'y a pas eu d'erreur on enregistre la nouvelle commande
            if(count($formError) === 0) {
                $em = $this->getDoctrine()->getManager();

                $em->persist($booking);

                foreach ($booking->getPeoples() as $people) {
                    $people->setBooking($booking);
                    $em->persist($people);
                }

                $em->flush();

                return $this->redirectToRoute('tlab_louvre_confirm', ['id'=>$booking->getId()]);
            }


        }

        return $this->render('TLABLouvreBundle:Booking:booking.html.twig', [
            'form'  => $form->createView(),
            'errors'    => $formError
        ]);
    }

    /**
     *
     * Permet de modifier une réservation
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editBookingAction($id, Request $request)
    {
        $validator = $this->get('validator');
        $repo = $this->getDoctrine()->getRepository('TLABLouvreBundle:Booking');

        // On créé une entité Booking
        $booking = $repo->find($id);
        // On crée le formulaire associé à Booking
        $form = $this->get('form.factory')->create(BookingType::class, $booking);
        $formError = '';

        // On récupère la requête lorsque le formulaire est posté
        $form->handleRequest($request);
        // Si les champs du formulaire sont valides
        if($form->isSubmitted() && $form->isValid()) {

            $limit = 0;
            $nbrPeoplesForThisDate = 0;

            // On récupère toutes les commandes pour la date sélectionnée
            $bookingsForThisDate = $repo->findBy([ 'date'=>$booking->getDate() ]);

            // On additionne toutes les personnes de chaque commande pour cette journée
            foreach ($bookingsForThisDate as $curr_booking) {
                if(!empty($curr_booking->getToken())) {
                    $nbrPeoplesForThisDate += $curr_booking->getNumberOfPeoples();
                }

            }

            // Si le nombre de personnes pour cette journée + le nombre de personnes de la commande dépasse la limite on déclenche une erreur
            $contrainst = new hasLowerThanThousandPeoples($nbrPeoplesForThisDate);
            $formError = $validator->validate($booking->getNumberOfPeoples(), $contrainst);

            // Si il n'y a pas eu d'erreur on enregistre la nouvelle commande
            if(count($formError) === 0) {
                $em = $this->getDoctrine()->getManager();

                $peopleRepo = $em->getRepository('TLABLouvreBundle:People');
                $storredPeoples = $peopleRepo->findBy(['booking'=>$booking]);

                foreach ($storredPeoples as $people) {
                    $em->remove($people);
                }

                $em->persist($booking);

                foreach ($booking->getPeoples() as $people) {
                    $people->setBooking($booking);
                    $em->persist($people);
                }

                $em->flush();

                return $this->redirectToRoute('tlab_louvre_confirm', ['id'=>$booking->getId()]);
            }


        }

        return $this->render('TLABLouvreBundle:Booking:booking.html.twig', [
            'form'  => $form->createView(),
            'errors'    => $formError
        ]);
    }

    /**
     * Permet de généré le résumé de la commande et gère le paiement via stripe
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function confirmAction($id, Request $request)
    {
        // On récupère toutes les commandes pour la date sélectionnée
        $repo = $this->getDoctrine()->getRepository('TLABLouvreBundle:Booking');
        $booking = $repo->find($id);

        // Pour chaque People, on définit le type de billet en fonction de celui du booking (pour calculer le prix de chaque personne)
        foreach ($booking->getPeoples() as $people) {
            $people->setType($booking->getType());
        }


        $paiement = new Paiement();
        // On crée le formulaire associé à Booking
        $form = $this->get('form.factory')->create(PaiementType::class, $paiement);

        $form->handleRequest($request);

        if( $form->isSubmitted() ) {

            if($form->isValid()) {

                Stripe::setApiKey('sk_test_VmsSrI1HDHks7bBCky5SsvKl');

                try {
                    // On récupère le montant de la commande
                    $charge = Charge::create(array('amount' => $booking->getTotalPrice()*100, 'currency' => 'eur', 'source' => $paiement->getToken() ));

                    $booking->setToken($paiement->getToken());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($booking);
                    $em->flush();

                    // On définit le mail
                    $contenu = $this->renderView('TLABLouvreBundle:Ticket:email.html.twig', array(
                        'token' => $paiement->getToken()
                    ));


                    $mailer = $this->get('swiftmailer.mailer');

                    $message = (new Swift_Message('Musée du Louvre: Votre billet d\'entrée'))
                        ->setFrom(['becerra.mail@gmail.com' => 'Musée du Louvre'])
                        ->setTo([$booking->getEmail()])
                        ->setContentType('text/html')
                        ->setBody($contenu)
                    ;

                    $result = $mailer->send($message);


                    return $this->redirectToRoute('tlab_louvre_success');

                } catch (Card $error) {

                    // Si il y a eu une erreur on la renvoi en flashbag
                    $request->getSession()->getFlashBag()->add('paiement_error', $error->getMessage());
                }

                

            } else { // SI le formulaire n'est pas valide
                    // On ajoute un flashbag d'erreur
            }

        }



        return $this->render('TLABLouvreBundle:Booking:confirm.html.twig', [
            'booking'   => $booking,
            'form'   => $form->createView()
        ]);
    }

    public function successAction()
    {
        return $this->render('TLABLouvreBundle:Booking:success.html.twig', [
        ]);
    }
}
