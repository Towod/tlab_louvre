<?php

namespace TLAB\LouvreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('date', DateType::class, [
                'widget'    => 'single_text'
            ])
            ->add('type', ChoiceType::class, [
                'choices'=> [
                    'Journée complète'  => '1',
                    'Demi-Journée'      => '2',
                ],
            ])
            ->add('peoples', CollectionType::class, [
                'entry_type' => PeopleType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype'    => 'TLABLouvreBundle:People:prototype.html.twig',
            ])
            ->add('submit', SubmitType::class)
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TLAB\LouvreBundle\Entity\Booking'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'booking';
    }


}
