<?php

namespace TLAB\LouvreBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class hasLowerThanThousandPeoplesValidator extends ConstraintValidator
{


    public function validate($value, Constraint $constraint)
    {

            $limit = 1000;

            if($value + $constraint->getNbrPeoples() > $limit) {
                $this->context->addViolation($constraint->message);
            }



    }


}