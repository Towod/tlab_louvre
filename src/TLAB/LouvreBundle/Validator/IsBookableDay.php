<?php

namespace TLAB\LouvreBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsBookableDay extends Constraint
{
    public $message = "Votre billeterie en ligne ne vous permet pas de réserver un dimanche ou un jour férié. Sélectionnez une autre date ou rendez-vous sur place pour réserver votre visite.";
}