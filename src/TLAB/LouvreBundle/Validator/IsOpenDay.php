<?php

namespace TLAB\LouvreBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsOpenDay extends Constraint
{
    public $message = "Votre musée est fermé le mardi ainsi que les 1er mai, 1er novembre et 25 décembre. Merci de sélectionner une autre date.";
}