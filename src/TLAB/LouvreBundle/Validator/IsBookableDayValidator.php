<?php

namespace TLAB\LouvreBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class IsBookableDayValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $year = $value->format('Y');
        $easterDateTime =  $this->resetEasterDateTime($year);

        // On récupère le LUNDI de paques
        $easter_monday = $this->getEasterMondayDateTime($easterDateTime);

        // On reset la date de paque
        $easterDateTime = $this->resetEasterDateTime($year);

        // On récupère le JEUDI de paques (40 jours après Paques (src: Wikipedia)
        $ascension = $this->getEasterThursdayDateTime($easterDateTime);

        // On reset la date de paque
        $easterDateTime = $this->resetEasterDateTime($year);

        // On récupère le lundi de Pentecôte (7 semaines après le lundi de Paques (src: Wikipedia)
        $pentecote_monday = $this->getPentecoteMondayDateTime( $this->getEasterMondayDateTime($easterDateTime) );

        // On reset la date de paque
        $easterDateTime = $this->resetEasterDateTime($year);

        $interval = new \DateInterval('P1D');

        // Liste des jours fériés où le musée est ouvert (mais où l'on ne peut pas reserver en ligne)
        $forbid_days = [

            // Jours variables
            'easter_monday' => $easter_monday->format('d/m'),
            'ascension' => $ascension->format('d/m'),
            'pentecote_monday' => $pentecote_monday->format('d/m'),

            // Jours fixes
            'new_year' => '01/01',
            'may_08_1945' => '08/05',
            'july_14' => '14/07',
            'assomption' => '15/08',
            'armistice' => '11/11',

            // Dimanches
            'sunday' => 'Sun',
        ];

        $day = $value->format('D');

        foreach ($forbid_days as $forbid_day) {

            if($value->format('d/m') == $forbid_day || $day == $forbid_day) {
                $this->context->addViolation($constraint->message);
            }
        }



    }

    protected function resetEasterDateTime( $year)
    {
        return new \DateTime(date('d-m-Y', easter_date($year)));
    }

    /**
     * Retourne la date du lundi de paques pour chaque année en France
     *
     * @return \DateTime|static
     */
    protected function getEasterMondayDateTime($easterDateTime)
    {
        // On définit une intervalle d'une journée
        $interval = new \DateInterval('P1D');

        // Tant que le jour n'est pas Lundi on ajoute l'intervale à la Date
        while($easterDateTime->format('D') != 'Mon') {
            $easterDateTime = $easterDateTime->add($interval);
        }

        // On renvoi la date du lundi de Paques
        return $easterDateTime;
    }

    /**
     * Retourne la date du JEUDI de paques pour chaque année en France
     *
     * @return \DateTime|static
     */
    protected function getEasterThursdayDateTime($easterDateTime)
    {
        // On définit une intervalle d'une journée
        $interval = new \DateInterval('P1D');

        // On définit une intervalle de 40 jours
        $interval_forty = new \DateInterval('P40D');

        $easterDateTime->add($interval_forty);

        // On renvoi la date du lundi de Paques
        return $easterDateTime;
    }

    /**
     * Retourne la date du LUNDI de Pentecôte pour chaque année en France
     *
     */
    protected function getPentecoteMondayDateTime( $easterDateTime)
    {

        // On définit une intervalle de 7 semaines
        $interval_seven_weeks = new \DateInterval('P49D');

        $easterDateTime->add($interval_seven_weeks);

        // On renvoi la date
        return $easterDateTime;
    }
}