<?php

namespace TLAB\LouvreBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class hasLowerThanThousandPeoples extends Constraint
{
    public $nbrBookingForDate;
    public $message = "Il n'y a plus assez de places disponibles pour cette date. Veuillez sélectionner une nouvelle date.";

    public function __construct( $nbrBookingForDate )
    {
        $this->nbrBookingForDate = $nbrBookingForDate;
    }

    public function getNbrPeoples()
    {
        return $this->nbrBookingForDate;
    }



}