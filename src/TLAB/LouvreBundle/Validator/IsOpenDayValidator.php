<?php

namespace TLAB\LouvreBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class IsOpenDayValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {

        $day = $value->format('D');
        $day_month = $value->format('d/m');

        // On définit les pattern des dates où le musée est fermé
        $closedDates = [
            'tuesday'       => 'Tue',
            'may_01'        => '01/05',
            'november_01'   => '01/11',
            'december_25'   => '25/12',
        ];

        foreach ($closedDates as $closedDate) {


            if( $day == $closedDate || $day_month == $closedDate ) {
                $this->context->addViolation($constraint->message);
                return false;
            }
        }
    }
}