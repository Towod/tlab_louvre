<?php

namespace TLAB\LouvreBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use TLAB\LouvreBundle\Validator\IsOpenDay;
use TLAB\LouvreBundle\Validator\IsBookableDay;
use TLAB\LouvreBundle\Validator\CheckForTodayType;

/**
 * Booking
 *
 * @ORM\Table(name="booking")
 * @ORM\Entity(repositoryClass="TLAB\LouvreBundle\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     *
     * On vérifie que le champ n'est pas vide
     * @Assert\NotBlank(message="L'adresse e-mail ne peut être vide")
     *
     * On vérifie que nous recevons bien une adresse e-mail
     * @Assert\Email(message="Le format de votre adresse e-mail est incorrect")
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     *
     * On vérifie que le champ n'est pas vide
     * @Assert\NotBlank()
     *
     * On vérifie que nous recevons bien une date
     * @Assert\Date()
     *
     * On vérifie que la date sélectionnée est supèrieure ou égale à la date du jour
     * @Assert\GreaterThanOrEqual("today", message="La date ne peut être antérieure à celle d'aujourd'hui")
     *
     * On vérifie que le musée est ouvert pour le jour sélectionné
     * Le musée est fermé tous les mardi ainsi que les 1er mai, 1er novembre et 25 décembre
     * @IsOpenDay()
     *
     * On vérifie que le jour sélectionné est "Réservable"
     * Les jours non réservables sont les dimanches et jours fériés non listé dans la validation précédente
     * @IsBookableDay()
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")

     * On vérifie que le champ n'est pas vide
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="TLAB\LouvreBundle\Entity\People", mappedBy="booking", cascade={"persist"})
     * @Assert\Count(
     *      min = 1,
     *      max = 1000,
     *      minMessage = "Votre commande doit concerner au moins une personne",
     *      maxMessage = "Vous ne pouvez pas réserver plus de {{ limit }} places en une seule commande"
     * )
     * @Assert\Valid()
     */
    private $peoples;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=true)
     */
    private $token;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->peoples = new \Doctrine\Common\Collections\ArrayCollection();
        date_default_timezone_set('Europe/Paris');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Booking
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Booking
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Booking
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Add people
     *
     * @param \TLAB\LouvreBundle\Entity\People $people
     *
     * @return Booking
     */
    public function addPeople(\TLAB\LouvreBundle\Entity\People $people)
    {
        $this->peoples[] = $people;

        return $this;
    }

    /**
     * Remove people
     *
     * @param \TLAB\LouvreBundle\Entity\People $people
     */
    public function removePeople(\TLAB\LouvreBundle\Entity\People $people)
    {
        $this->peoples->removeElement($people);
    }

    /**
     * Get peoples
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeoples()
    {
        return $this->peoples;
    }

    /**
     * Get Number Of Peoples
     *
     * @return integer
     */
    public function getNumberOfPeoples()
    {
        return $this->peoples->count();
    }

    /**
     * Get total price
     *
     * @return integer
     */
    public function getTotalPrice()
    {
        $price = 0;

        foreach ($this->peoples as $people) {
            $price += $people->getPrice();
        }


        return $price;
    }

    /**
     * Si le jour selectionné est aujourd'hui et qu'il est plus de 14h
     * Alors le type de billet doit être demi-journée
     * @Assert\Callback
     */
    public function isGoodTypeForTodayPM(ExecutionContextInterface $context)
    {
        $now = new \DateTime("now"); // On récupère le DateTime actuel
        $pm = 14; // A partir de 14h on ne peut plus réserver de billet journée complète

        // Si la date sélectionnée dans le formulaire est aujourd'hui
        if($this->date->format('d/m/y') === $now->format('d/m/y') ) {

            // Si l'heure actuelle est 14H00 ou plus
            if($now->format('H') >= $pm) {

                // Si le type de billet est journée complète
                if($this->type == '1') {

                    // Alors on déclenche une erreur
                    $context
                        ->buildViolation('La matinée est déja passée pour aujourd\'hui. Veuillez sélectionner des billets demi-journée.')
                        ->atPath('type')
                        ->addViolation()
                    ;
                }
            }
        }
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Booking
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}
