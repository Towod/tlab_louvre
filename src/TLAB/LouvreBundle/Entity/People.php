<?php

namespace TLAB\LouvreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * People
 *
 * @ORM\Table(name="people")
 * @ORM\Entity(repositoryClass="TLAB\LouvreBundle\Repository\PeopleRepository")
 */
class People
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=50)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=3,
     *     max=50,
     *     minMessage="Le prénom doit contenir au moins 3 caractères",
     *     maxMessage="Le prénom doit contenir un maximum de 50 caractères"
     * )
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=50)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=3,
     *     max=50,
     *     minMessage="Le nom de famille doit contenir au moins 3 caractères",
     *     maxMessage="Le nom de famille doit contenir un maximum de 50 caractères"
     * )
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=50)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=2,
     *     max=2,
     *     minMessage="La valeur reçue pour le pays ne peut contenir moins de deux caractères",
     *     maxMessage="La valeur reçue pour le pays ne peut contenir plus de deux caractères"
     * )
     */
    private $country;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date")
     *
     * @Assert\NotBlank()
     * @Assert\Date()
     * @Assert\LessThan("today", message="La date de naissance ne peut être postèrieure à celle d'aujourd'hui")
     */
    private $birthday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="reduced_price", type="boolean", nullable=true)
     */
    private $reducedPrice;

    /**
     * @ORM\ManyToOne(targetEntity="TLAB\LouvreBundle\Entity\Booking", inversedBy="peoples", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $booking;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fistname
     *
     * @param string $fistname
     *
     * @return People
     */
    public function setFistname($fistname)
    {
        $this->fistname = $fistname;

        return $this;
    }

    /**
     * Get fistname
     *
     * @return string
     */
    public function getFistname()
    {
        return $this->fistname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return People
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return People
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return People
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set reducedPrice
     *
     * @param boolean $reducedPrice
     *
     * @return People
     */
    public function setReducedPrice($reducedPrice)
    {
        $this->reducedPrice = $reducedPrice;

        return $this;
    }

    /**
     * Get reducedPrice
     *
     * @return boolean
     */
    public function getReducedPrice()
    {
        return $this->reducedPrice;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return People
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set booking
     *
     * @param \TLAB\LouvreBundle\Entity\Booking $booking
     *
     * @return People
     */
    public function setBooking(\TLAB\LouvreBundle\Entity\Booking $booking)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \TLAB\LouvreBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * Get age
     *
     * @return integer
     */
    public function getAge()
    {
        $age = 0;

        $now = new \DateTime("now");

        $diff = $now->diff($this->birthday);

        return $diff->y;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        // On fixe le prix à 0 pour les enfants de moins de 4 ans
        $price = 0;

        // On définit le prix en fonction de l'age
        if($this->getAge() >= 12 && $this->getAge() < 60 )      $price = 16;
        if($this->getAge() >= 4 && $this->getAge() < 12 )       $price = 8;
        if($this->getAge() >= 60 )                              $price = 12;

        // Si il y a un tarif réduit, on applique les -10€
        if($this->reducedPrice)                                 $price -= 10;

        // Si c'est une demi-journée on casse le prix en deux
        if($this->type == 2) $price /= 2;

        return $price;
    }

    protected $type;

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}
