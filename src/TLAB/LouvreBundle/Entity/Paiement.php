<?php

namespace TLAB\LouvreBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Paiement
 *
 */
class Paiement
{
    private $id;
    private $token;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }
}

